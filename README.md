

This is a java client library for Heal APIs relevant for HNF event handlers

* Import client library
    
            <dependency>
                <groupId>com.appnomic.appsone</groupId>
                <artifactId>event-handler-clientlib</artifactId>
                <version>1.0.0-SNAPSHOT</version>
            </dependency>
            
            
 
 
* Usage Examples
 
    Get a token
    
    `TokenPojo token = Token.getToken();`  
       
    Get list of services

    `String accountId =  "d681ef13-d690-4917-jkhg-6c79b-1";`
    
     `Service service =  new Service();`
   
     `List<ServicePojo> list  = service.getServices(token, accountId);`
       
    Get list of applications
 
    `Application application = new Application();`  
     
    `List<ApplicationPojo>  list = application.getApplications(token, accountId);`
    
    Add a new category
    
     `CategoryPojo categoryPojo = CategoryPojo.builder()
                                      .name("NewCategory")
                                      .identifier("NewCategoryId")
                                      .isWorkLoad(true).build();`
        
     `Category category = new Category();`
       
     `boolean categoryAdded = category.addCategory(accountIdentifier, categoryPojo, token);` 

    Add a new service
    
    `Service service = new Service();`
           
    `ServiceRequestPojo serviceRequestPojo = ServiceRequestPojo.builder()
                           .name("client-lib-serv").identifier("random-identifier")
                           .layer("bottom").type("exotic").build();`
                           
    `List<ServiceRequestPojo> serviceList = new ArrayList<>();`
             
    `serviceList.add(serviceRequestPojo);`
             
    `boolean serviceAdded = service.addService(accountIdentifier, serviceList, token);`


   Get List of Instances
    
    `Instances instances = new Instances();`
    `instances.getInstances(token, accountIdentifier, 15);`
   
   Add a new component instance
   
    `Instances instances = new Instances();`
        
    `List<String> serviceIdentifiers = new ArrayList<>();`
   
    `serviceIdentifiers.add("4f51f8b0-b6c9-4375-8729-b21bc6d88888");`
   
    `List<ComponentInstanceResponsePojo> componentInstances = new ArrayList<>();
   
    `ComponentInstanceResponsePojo compInstance = ComponentInstanceResponsePojo.builder()
                   .name("CompClientLib").identifier("4f51f8b0-b6c9-4375-8729-b21bc6d809999")
                   .componentName("AIX").componentVersion("6.1")
                   .serviceIdentifiers(serviceIdentifiers).build();`
   
    `componentInstances.add(compInstance);`
   
    `boolean instanceAdded =instances.addInstance(accountIdentifier, componentInstances, token);`
   
   Get a list of KPI
  
   `KPI kpi = new KPI();`
   
   `kpi.getKPIs(token, accountIdentifier,  18,  1,  1 );`
   
   Add a new KPI
   
    `KPIPojo kpiPojo = KPIPojo.builder()
                      .kpiName("Pod CPU Current").kpiIdentifier("POD_CPU_CUR_ID")
                      .description("Node Current CPU usage").groupKpiIdentifier("None")
                      .dataType("Float").kpiType("Core").clusterOperation("None")
                      .rollupOperation("None").instanceAggregation("None")
                      .clusterAggregation("None").collectionIntervalSeconds(60)
                      .measureUnits("%").componentName("Node").componentType("Host")
                      .componentVersion("1.12.0").categoryName("CPU")
                      .enableAnalytics(1).build();`
    
    `KPI kpi = new KPI();`
    
    `boolean kpiAdded = kpi.addKPI(accountIdentifier, kpiPojo, token);`
    
   Add a Group KPI
   
   `GroupKPIPojo groupKPIPojo = GroupKPIPojo.builder()
                   .groupKpiName("GROUP_KPI_NAME").groupKpiIdentifier("GROUP_KPI_UNIQUE_IDENTIFIER")
                   .description("Description of the KPI Group").kpiType("CORE")
                   .discovery(0).build();`
   `KPI kpi = new KPI();`
   
   `boolean groupKpiAdded = kpi.addGroupKPI(accountIdentifier, groupKPIPojo, token);`
   
   
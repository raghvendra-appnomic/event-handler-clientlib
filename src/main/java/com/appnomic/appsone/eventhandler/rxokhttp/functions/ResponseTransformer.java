package com.appnomic.appsone.eventhandler.rxokhttp.functions;

import com.appnomic.appsone.eventhandler.rxokhttp.HttpStatus;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

public interface ResponseTransformer<R> extends IoFunction<Response, R> {

    static ResponseTransformer<Response> identity() {
        return t -> t;
    }

    static ResponseTransformer<HttpStatus> httpStatus() {
        return response -> HttpStatus.of(response.code(), response.message());
    }

    static <T> ResponseTransformer<T> fromBody(final ResponseBodyTransformer<T> bodyTransformer) {
        return response -> {
            try (ResponseBody body = response.body()) {
                return bodyTransformer.apply(body);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }
}

package com.appnomic.appsone.eventhandler.rxokhttp;

import com.appnomic.appsone.eventhandler.pojo.request.CategoryPojo;
import com.appnomic.appsone.eventhandler.pojo.request.GroupKPIPojo;
import com.appnomic.appsone.eventhandler.pojo.request.KPIPojo;
import com.appnomic.appsone.eventhandler.pojo.request.ServiceRequestPojo;
import com.appnomic.appsone.eventhandler.pojo.response.*;
import com.appnomic.appsone.eventhandler.rxokhttp.functions.*;
import io.reactivex.rxjava3.core.Observable;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okio.Buffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

class OkHttpBasedRxHttpClient implements RxHttpClient {

    private final Logger logger = LoggerFactory.getLogger(OkHttpBasedRxHttpClient.class);

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType OCTET = MediaType.parse("application/octet-stream; charset=utf-8");
    public static final MediaType TAR = MediaType.parse("application/tar; charset=utf-8");

    private final DefaultOkHttpBasedRxHttpClient client;

    OkHttpBasedRxHttpClient(final String baseApiUrl, final ClientConfig clientConfig) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        setClientConfig(clientConfig, clientBuilder);
        client = new DefaultOkHttpBasedRxHttpClient(baseApiUrl, clientBuilder.build(), RxHttpClient::fullEndpointUrl);
    }

    OkHttpBasedRxHttpClient(final String host, final int port, ClientConfig clientConfig) {
        this(host, port, Optional.empty(), clientConfig);
    }

    OkHttpBasedRxHttpClient(final String host, final int port, final Optional<String> certPath, ClientConfig clientConfig) {
        final String scheme = certPath.isPresent() ? "https" : "http";
        String baseApiUrl = scheme + "://" + host + ":" + port;
        logger.info("Base API uri {}", baseApiUrl);
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        if (certPath.isPresent()) {
            clientBuilder.sslSocketFactory(new SslCertificates(Paths.get(certPath.get())).sslContext().getSocketFactory());
        }
        setClientConfig(clientConfig, clientBuilder);
        client = new DefaultOkHttpBasedRxHttpClient(baseApiUrl, clientBuilder.build(), RxHttpClient::fullEndpointUrl);
    }

    private void setClientConfig(ClientConfig clientConfig, OkHttpClient.Builder clientBuilder) {
        clientBuilder.followRedirects(clientConfig.isFollowRedirects());
        clientBuilder.followSslRedirects(clientConfig.isFollowSslRedirects());
        clientBuilder.retryOnConnectionFailure(clientConfig.isRetryOnConnectionFailure());
        Duration readTimeout = clientConfig.getReadTimeout();
        if (readTimeout != null) {
            clientBuilder.readTimeout(readTimeout.getSeconds(), TimeUnit.SECONDS);
        }
        Duration writeTimeout = clientConfig.getWriteTimeout();
        if (writeTimeout != null) {
            clientBuilder.writeTimeout(writeTimeout.getSeconds(), TimeUnit.SECONDS);
        }
        Duration connectTimeout = clientConfig.getConnectTimeout();
        if (connectTimeout != null) {
            clientBuilder.connectTimeout(connectTimeout.getSeconds(), TimeUnit.SECONDS);
        }
    }


    @Override
    public Observable<String> get(String endpoint, QueryParameter... queryParameters) {
        return client.get(endpoint, queryParameters);
    }

    @Override
    public Observable<String> get(String endpoint, Map<String, String> headers, QueryParameter... queryParameters) {
        return client.get(endpoint, headers, queryParameters);
    }

    @Override
    public <R> Observable<R> get(String endpoint, StringResponseTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.get(endpoint, transformer, queryParameters);
    }

    @Override
    public <R> Observable<R> get(String endpoint, Map<String, String> headers, StringResponseTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.get(endpoint, headers, transformer, queryParameters);
    }

    @Override
    public <R> Observable<R> get(String endpoint, StringResponseToCollectionTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.get(endpoint, transformer, queryParameters);
    }

    @Override
    public <R> Observable<R> get(String endpoint, Map<String, String> headers, StringResponseToCollectionTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.get(endpoint, headers, transformer, queryParameters);
    }

    @Override
    public <R> Observable<R> get(String endpoint, ResponseTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.get(endpoint, transformer, queryParameters);
    }

    @Override
    public Observable<String> getResponseStream(String endpoint, QueryParameter... queryParameters) {
        return client.getResponseStream(endpoint, queryParameters);
    }

    @Override
    public Observable<String> getResponseStream(String endpoint, Map<String, String> headers, QueryParameter... queryParameters) {
        return client.getResponseStream(endpoint, headers, queryParameters);
    }

    @Override
    public <T> Observable<T> getResponseStream(String endpoint, Map<String, String> headers, StringResponseTransformer<T> transformer, QueryParameter... queryParameters) {
        return client.getResponseStream(endpoint, headers, transformer, queryParameters);
    }

    @Override
    public Observable<Buffer> getResponseBufferStream(String endpoint, QueryParameter... queryParameters) {
        return client.getResponseBufferStream(endpoint, queryParameters);
    }

    @Override
    public <T> Observable<T> getResponseStream(String endpoint, StringResponseTransformer<T> transformer, QueryParameter... queryParameters) {
        return client.getResponseStream(endpoint, transformer, queryParameters);
    }

    @Override
    public Observable<HttpStatus> getResponseHttpStatus(String endpointPath, QueryParameter... queryParameters) {
        return client.getResponseHttpStatus(endpointPath, queryParameters);
    }

    @Override
    public Observable<HttpStatus> post(String endpoint, QueryParameter... queryParameters) {
        return client.post(endpoint, queryParameters);
    }

    @Override
    public Observable<HttpStatus> post(String endpoint, Map<String, String> headers, QueryParameter... queryParameters) {
        return client.post(endpoint, headers, queryParameters);
    }

    @Override
    public Observable<HttpStatus> post(String endpoint, String body, QueryParameter... queryParameters) {
        return client.post(endpoint, body, queryParameters);
    }

    @Override
    public Observable<HttpStatus> post(String endpoint, Map<String, String> headers, String body, QueryParameter... queryParameters) {
        return client.post(endpoint, headers, body, queryParameters);
    }

    @Override
    public <R> Observable<R> post(String endpoint, ResponseBodyTransformer<R> bodyTransformer, QueryParameter... queryParameters) {
        return client.post(endpoint, bodyTransformer, queryParameters);
    }

    @Override
    public <R> Observable<R> post(String endpoint, String postBody, ResponseBodyTransformer<R> bodyTransformer, QueryParameter... queryParameters) {
        return client.post(endpoint, postBody, bodyTransformer, queryParameters);
    }

    @Override
    public <R> Observable<R> post(String endpoint, Map<String, String> headers, ResponseBodyTransformer<R> bodyTransformer, QueryParameter... queryParameters) {
        return client.post(endpoint, headers, bodyTransformer, queryParameters);
    }

    @Override
    public <R> Observable<R> post(String endpoint, String postBody, ResponseTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.post(endpoint, postBody, transformer, queryParameters);
    }

    @Override
    public <R> Observable<R> post(String endpoint, Map<String, String> headers, String postBody, ResponseTransformer<R> transformer, QueryParameter... queryParameters) {
        return client.post(endpoint, headers, postBody, transformer, queryParameters);
    }

    @Override
    public Observable<String> postAndReceiveResponse(String endpoint, QueryParameter... queryParameters) {
        return client.postAndReceiveResponse(endpoint, queryParameters);
    }

    @Override
    public Observable<String> postAndReceiveResponse(String endpoint, Map<String, String> headers, QueryParameter... queryParameters) {
        return client.postAndReceiveResponse(endpoint, headers, queryParameters);
    }

    @Override
    public Observable<String> postAndReceiveResponse(String endpoint, Map<String, String> headers, Predicate<String> errorChecker, QueryParameter... queryParameters) {
        return client.postAndReceiveResponse(endpoint, headers, errorChecker, queryParameters);
    }

    @Override
    public Observable<String> postAndReceiveResponse(String endpoint, Map<String, String> headers, String postBody, Predicate<String> errorChecker, QueryParameter... queryParameters) {
        return client.postAndReceiveResponse(endpoint, headers, postBody, errorChecker, queryParameters);
    }

    @Override
    public Observable<String> postAndReceiveStream(String endpoint, String postBody, QueryParameter... queryParameters) {
        return client.postAndReceiveStream(endpoint, postBody, queryParameters);
    }

    @Override
    public Observable<String> postAndReceiveStream(String endpoint, Map<String, String> headers, String postBody, QueryParameter... queryParameters) {
        return client.postAndReceiveStream(endpoint, headers, postBody, queryParameters);
    }

    @Override
    public <R> Observable<R> postTarStream(String endpoint, Path pathToTarArchive, BufferTransformer<R> transformer) {
        return client.postTarStream(endpoint, pathToTarArchive, transformer);
    }

    @Override
    public <R> Observable<R> postTarStream(String endpoint, Path pathToTarArchive, ResponseTransformer<R> transformer) {
        return client.postTarStream(endpoint, pathToTarArchive, transformer);
    }

    @Override
    public Observable<HttpStatus> postTarStream(String endpoint, Path pathToTarArchive) {
        return client.postTarStream(endpoint, pathToTarArchive);
    }

    @Override
    public Observable<TokenPojo> getToken() {
        return client.getToken();
    }

    @Override
    public Observable<Boolean> addService(String identifier, List<ServiceRequestPojo> services, TokenPojo tokenPojo) {
        return client.addService(identifier,services,tokenPojo);
    }

    @Override
    public Observable<List<ServiceResponsePojo>> getServices(TokenPojo tokenPojo, String identifier) {
        return client.getServices(tokenPojo,identifier);
    }

    @Override
    public Observable<List<ApplicationResponsePojo>> getApplications(TokenPojo tokenPojo, String identifier) {
        return client.getApplications(tokenPojo, identifier);
    }

    @Override
    public Observable<List<AccountConfiguration>> getTxnConfigData() {
        return client.getTxnConfigData();
    }

    @Override
    public Observable<List<AccountConfiguration>> getAgentConfigData(String agentIdentifier) {
        return client.getAgentConfigData(agentIdentifier);
    }

    @Override
    public Observable<List<ComponentInstanceResponsePojo>> getInstances(TokenPojo token, String accountIdentifier, int serviceId) {
         return client.getInstances(token, accountIdentifier, serviceId);
    }

    @Override
    public Observable<List<KpiNamesPojo>> getKPIs(TokenPojo token, String accountIdentifier, int serviceId, int instanceId, int categoryId) {
         return client.getKPIs(token, accountIdentifier, serviceId, instanceId, categoryId );
    }

    @Override
    public Observable<Boolean> addCategory(String accountIdentifier, CategoryPojo categoryPojo, TokenPojo token) {
        return client.addCategory(accountIdentifier, categoryPojo, token);
    }

    @Override
    public Observable<Boolean> addKPI(String accountIdentifier, KPIPojo kpiPojo, TokenPojo token) {
        return client.addKPI(accountIdentifier, kpiPojo, token);
    }

    @Override
    public Observable<Boolean> addGroupKPI(String accountIdentifier, GroupKPIPojo groupKPIPojo, TokenPojo token) {
        return client.addGroupKPI(accountIdentifier, groupKPIPojo, token);
    }

    @Override
    public Observable<HttpStatus> delete(String endpoint, QueryParameter... queryParameters) {
        return client.delete(endpoint, queryParameters);
    }

    @Override
    public Observable<HttpStatus> delete(String endpoint, Map<String, String> headers, QueryParameter... queryParameters) {
        return client.delete(endpoint, headers, queryParameters);
    }

    @Override
    public Observable<Response> head(String endpoint, QueryParameter... queryParameters) {
        return client.head(endpoint, queryParameters);
    }

    @Override
    public Observable<Response> head(String endpoint, Map<String, String> headers, QueryParameter... queryParameters) {
        return client.head(endpoint, headers, queryParameters);
    }
}
package com.appnomic.appsone.eventhandler.rxokhttp.functions;

import okio.Buffer;

import java.util.function.Function;

public interface BufferTransformer<T> extends Function<Buffer, T> {

    static BufferTransformer<Buffer> identityOp() {
        return t -> t;
    }
}

package com.appnomic.appsone.eventhandler.rxokhttp.functions;

import com.appnomic.appsone.eventhandler.rxokhttp.HttpStatus;
import io.reactivex.rxjava3.core.Observable;

import java.util.function.BiFunction;

@FunctionalInterface
public interface HttpExecutionFunction extends BiFunction<String, String, Observable<HttpStatus>> {
}

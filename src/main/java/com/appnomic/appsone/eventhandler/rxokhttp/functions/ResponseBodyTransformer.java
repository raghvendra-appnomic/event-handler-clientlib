package com.appnomic.appsone.eventhandler.rxokhttp.functions;


import okhttp3.ResponseBody;

public interface ResponseBodyTransformer<R> extends IoFunction<ResponseBody, R> {

}

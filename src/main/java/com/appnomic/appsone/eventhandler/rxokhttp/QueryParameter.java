package com.appnomic.appsone.eventhandler.rxokhttp;

public class QueryParameter {

    private final String param;
    private final Object value;

    private QueryParameter(String param, Object value) {
        this.param = param;
        this.value = value;
    }

    public static QueryParameter of(String param, Object value) {
        return new QueryParameter(param, value);
    }

    public String param() {
        return param;
    }

    public Object value() {
        return value;
    }
}

package com.appnomic.appsone.eventhandler.rxokhttp.functions;

import com.appnomic.appsone.eventhandler.rxokhttp.QueryParameter;
import okhttp3.HttpUrl;

public interface HttpUrlFunction {

    HttpUrl apply(String baseApiUrl, String endpoint, QueryParameter... queryParameters);
}

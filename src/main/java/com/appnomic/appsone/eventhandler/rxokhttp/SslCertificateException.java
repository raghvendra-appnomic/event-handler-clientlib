package com.appnomic.appsone.eventhandler.rxokhttp;

public class SslCertificateException extends RuntimeException {

    public SslCertificateException(final String message) {
        super(message);
    }

    public SslCertificateException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SslCertificateException(final Throwable cause) {
        super(cause);
    }
}

package com.appnomic.appsone.eventhandler.rxokhttp;

import io.reactivex.rxjava3.subscribers.TestSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpStatusSubscriber extends TestSubscriber<String> {

    private final Logger logger = LoggerFactory.getLogger(HttpStatusSubscriber.class);

    private HttpStatus status = null;

    @Override
    public void onComplete() {
        logger.info("Successfully processed all events");
        status = HttpStatus.OK;
    }

    @Override
    public void onError(Throwable e) {
        logger.error("Error encountered >> ", e);
        if (e instanceof ServiceException) {
            status = HttpStatus.of(((ServiceException) e).getCode(), ((ServiceException) e).getHttpMessage());
        } else {
            status = HttpStatus.SERVER_ERROR;
        }
    }

    @Override
    public void onNext(String res) {
        logger.info("Received message >> {}", res);
    }

    public HttpStatus getStatus() {
        return status;
    }
}

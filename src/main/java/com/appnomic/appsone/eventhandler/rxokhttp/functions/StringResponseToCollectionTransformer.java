package com.appnomic.appsone.eventhandler.rxokhttp.functions;

import java.util.Collection;
import java.util.function.Function;

/**
 * A function to convert a String response into a Collection
 *
 * @param <T> return value type
 */
@FunctionalInterface
public interface StringResponseToCollectionTransformer<T> extends Function<String, Collection<T>> {

}

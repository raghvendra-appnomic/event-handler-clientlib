package com.appnomic.appsone.eventhandler.rxokhttp;

public class StreamResponseException extends RuntimeException {

    private final String json;

    public StreamResponseException(String json) {
        this.json = json;
    }

    public String getJson() {
        return json;
    }
}



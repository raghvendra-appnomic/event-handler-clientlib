package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.pojo.response.ApplicationResponsePojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.appsone.eventhandler.util.APIConfigLoader;
import com.appnomic.appsone.eventhandler.util.CommonUtils;
import com.appnomic.appsone.eventhandler.util.Constants;
import com.appnomic.appsone.eventhandler.pojo.response.GenericResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application {
    public List<ApplicationResponsePojo> getApplications(TokenPojo tokenPojo, String identifier) {

        Map<String, String> replaceParams = new HashMap<>();
        replaceParams.put("identifier", identifier);

        Endpoint serviceEndpoint = APIConfigLoader.getEndpointById("applications-get", replaceParams, tokenPojo);

        Request request = API.buildRequest(serviceEndpoint, null);

        Response response;
        List<ApplicationResponsePojo> applications = null;
        try {
            response = API.sendRequest(request);
            if (response.code() == Constants.SUCCESS) {
                ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    GenericResponse<ApplicationResponsePojo> data = objectMapper.readValue(response.body().string(), new TypeReference<GenericResponse<ApplicationResponsePojo>>() {
                    });
                    applications = data.getData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return applications;
    }
}

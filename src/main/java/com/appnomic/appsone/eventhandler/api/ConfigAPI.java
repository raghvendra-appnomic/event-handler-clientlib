package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.pojo.response.AccountConfiguration;
import com.appnomic.appsone.eventhandler.pojo.response.GenericResponse;
import com.appnomic.appsone.eventhandler.util.APIConfigLoader;
import com.appnomic.appsone.eventhandler.util.CommonUtils;
import com.appnomic.appsone.eventhandler.util.Constants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Request;
import okhttp3.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigAPI {

    public List<AccountConfiguration> getTxnConfigData() {

        Endpoint txnConfigEndpoint = APIConfigLoader.getEndpointById("txn-config-data", null, null);

        Request request = API.buildRequest(txnConfigEndpoint, null);

        Response response;
        List<AccountConfiguration> txnConfigData = null;
        try {
            response = API.sendRequest(request);
            if (response.code() == Constants.SUCCESS) {
                ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    GenericResponse<AccountConfiguration> data = objectMapper.readValue(response.body().string(), new TypeReference<GenericResponse<AccountConfiguration>>() {
                    });
                    txnConfigData = data.getData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return txnConfigData;
    }

    public List<AccountConfiguration> getAgentConfigData(String agentIdentifier) {

        Map<String, String> replaceParams = new HashMap<>();
        replaceParams.put("identifier",agentIdentifier);

        Endpoint agentConfigEndpoint = APIConfigLoader.getEndpointById("agent-config-data", replaceParams, null);

        Request request = API.buildRequest(agentConfigEndpoint, null);

        Response response;
        List<AccountConfiguration> agentConfigData = null;
        try {
            response = API.sendRequest(request);
            if (response.code() == Constants.SUCCESS) {
                ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    GenericResponse<AccountConfiguration> data = objectMapper.readValue(response.body().string(), new TypeReference<GenericResponse<AccountConfiguration>>() {
                    });
                    agentConfigData = data.getData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return agentConfigData;
    }

}

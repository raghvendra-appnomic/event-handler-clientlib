package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.appsone.eventhandler.util.APIConfigLoader;
import com.appnomic.appsone.eventhandler.util.CommonUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;

public class Token {
    private static final ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();

    public static TokenPojo getToken() {
        TokenPojo token = null;
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Endpoint tokenEndpoint = APIConfigLoader.getEndpointById("token", null, null);

        if (tokenEndpoint != null) {

            MediaType mediaType = MediaType.parse(tokenEndpoint.getMediaType());
            String payloadStr = APIConfigLoader.getTokenPayload(tokenEndpoint);
            RequestBody body = RequestBody.create(mediaType, payloadStr);

            Request request = API.buildRequest(tokenEndpoint, body);

            Response response;
            try {
                response = API.sendRequest(request);
                try {
                    token = objectMapper.readValue(response.body().string(), TokenPojo.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return token;
    }
}

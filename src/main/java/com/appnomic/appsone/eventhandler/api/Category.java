package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.request.CategoryPojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.appsone.eventhandler.util.Constants;
import okhttp3.*;

import java.io.IOException;

public class Category {

    public boolean addCategory(String identifier, CategoryPojo categoryPojo, TokenPojo tokenPojo) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        String categoryRequestBody = prepareRequestBody(categoryPojo);
        RequestBody body = RequestBody.create(mediaType, categoryRequestBody);
        String url = Constants.CATEGORY_URL_POST.replace(":identifier", identifier);
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader("Authorization", tokenPojo.getAccess_token())
                .addHeader("Accept", "*/*")
                .addHeader("Content-Type", "text/plain")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == Constants.SUCCESS) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String prepareRequestBody(CategoryPojo categoryPojo) {
        String categoryRequestBody = "{\n    \"name\": \":name\", \n   " +
                " \"identifier\": \":identifier\", \n   " +
                " \"workLoad\": \":workLoad\" \n}";

        categoryRequestBody = categoryRequestBody.replace(":name", categoryPojo.getName());
        categoryRequestBody = categoryRequestBody.replace(":identifier", categoryPojo.getIdentifier());
        categoryRequestBody = categoryRequestBody.replace(":workLoad", String.valueOf(categoryPojo.isWorkLoad()));

        return categoryRequestBody;
    }
}


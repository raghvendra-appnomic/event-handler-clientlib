package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.Attributes;
import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.pojo.response.ComponentInstanceResponsePojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.appsone.eventhandler.util.APIConfigLoader;
import com.appnomic.appsone.eventhandler.util.CommonUtils;
import com.appnomic.appsone.eventhandler.util.Constants;
import com.appnomic.appsone.eventhandler.pojo.response.GenericResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Instances {
    public List<ComponentInstanceResponsePojo> getInstances(TokenPojo tokenPojo, String accountIdentifier, int serviceId) {

        Map<String, String> replaceParams = new HashMap<>();
        replaceParams.put("identifier",accountIdentifier);
        replaceParams.put("serviceId",String.valueOf(serviceId));

        Endpoint instanceEndpoint = APIConfigLoader.getEndpointById("instance-get", replaceParams, tokenPojo);

        Request request = API.buildRequest(instanceEndpoint,null);

        Response response;
        List<ComponentInstanceResponsePojo> instances = null;
        try {
            response = API.sendRequest(request);
            if (response.code() == Constants.SUCCESS) {
                ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    GenericResponse<ComponentInstanceResponsePojo> data = objectMapper.readValue(response.body().string(), new TypeReference<GenericResponse<ComponentInstanceResponsePojo>>() {
                    });
                    instances = data.getData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return instances;
    }

    public boolean addInstance(String identifier, List<ComponentInstanceResponsePojo> instances, TokenPojo tokenPojo) {
        Map<String, String> replaceParams = new HashMap<>();
        replaceParams.put("identifier", identifier);

        Endpoint instanceEndpoint = APIConfigLoader.getEndpointById("instance-post", replaceParams, tokenPojo);
        boolean instanceAdded = false;
        if (instanceEndpoint != null) {

            MediaType mediaType = MediaType.parse(instanceEndpoint.getMediaType());

            String addRequestBody = prepareRequestBody(instances);
            RequestBody body = RequestBody.create(mediaType, addRequestBody);

            Request request = API.buildRequest(instanceEndpoint, body);

            Response response;
            try {
                response = API.sendRequest(request);
                String responseBody = response.body().string();
                if (response.code() == Constants.SUCCESS) {
                    instanceAdded = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instanceAdded;
    }

    private String prepareRequestBody(List<ComponentInstanceResponsePojo> components) {
        String prefix = "[\n   ";
        String suffix = "\n   ]";
        String body = "";
        List<String> comps = new ArrayList<>();
        List<String> attrList = new ArrayList<>();
        for (ComponentInstanceResponsePojo component : components) {
            String mandatory = "{\n    \"name\": \":name\",\n    \"identifier\": \":identifier" +
                    "\",\n  " +
                    "  \"componentName\": \":componentName\",\n    \"componentVersion\": \":componentVersion\",\n   " +
                    " \"serviceIdentifiers\": [\n  :serviceIdentifiers  ";

            mandatory = mandatory.replace(":name", component.getName());
            mandatory = mandatory.replace(":identifier", component.getIdentifier());
            mandatory = mandatory.replace(":componentName", component.getComponentName());
            mandatory = mandatory.replace(":componentVersion", component.getComponentVersion());
            String ids = "";
            for (String serviceId : component.getServiceIdentifiers()) {
                ids += ids + "\"" + serviceId + "\",";
            }
            ids = ids.substring(0, ids.length() - 1);
            mandatory = mandatory.replace(":serviceIdentifiers", ids);
            mandatory = mandatory + "\n    ],\n" ;

            for (Attributes attribute : component.getAttributes()) {
                String attr = "{\n\"name\":" + " \""+attribute.getName() +"\",\n"+ "\"value\":" + " \""+attribute.getValue()+"\"\n}";
                attrList.add(attr);
            }
            mandatory = mandatory + "\"attributes\": [";
            for(String s : attrList){
                mandatory = mandatory + s + ",\n";
            }
            mandatory = mandatory.substring(0, mandatory.length() - 2);
            mandatory = mandatory + "] \n}";
            comps.add(mandatory);
        }
        body += prefix;
        for (String mandt : comps) {
            body += mandt + ",";
        }
        body = body.substring(0, body.length() - 1);
        body += suffix;
        return body;
    }
}

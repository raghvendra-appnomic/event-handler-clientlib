package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.response.*;
import com.appnomic.appsone.eventhandler.pojo.request.GroupKPIPojo;
import com.appnomic.appsone.eventhandler.pojo.request.KPIPojo;
import com.appnomic.appsone.eventhandler.util.CommonUtils;
import com.appnomic.appsone.eventhandler.util.Constants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.List;

public class KPI {

    public boolean addKPI(String identifier, KPIPojo kPIPojo, TokenPojo tokenPojo) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        String kpiRequestBody = prepareRequestBody(kPIPojo);
        RequestBody body = RequestBody.create(mediaType, kpiRequestBody);
        String url = Constants.KPI_URL_POST.replace(":identifier", identifier);
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader("Authorization", tokenPojo.getAccess_token())
                .addHeader("Accept", "*/*")
                .addHeader("Content-Type", "text/plain")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == Constants.SUCCESS) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String prepareRequestBody(KPIPojo kpiPojo) {
        String kpiRequestBody = "{\n    \"kpiName\": \":kpiName\", \n    \"kpiIdentifier\": \":kpiIdentifier\", \n   " +
                " \"description\": \":description\", \n    \"groupKpiIdentifier\" : \":groupKpiIdentifier\", \n    " +
                "\"dataType\" : \":dataType\", \n    \"kpiType\": \":kpiType\",\n    \"clusterOperation\" : \":clusterOperation\", \n  " +
                "  \"rollupOperation\" : \":rollupOperation\", \n    \"instanceAggregation\" : \":instanceAggregation\", \n   " +
                " \"clusterAggregation\" : \":clusterAggregation\", \n    \"collectionIntervalSeconds\" : :collectionIntervalSeconds, \n  " +
                "  \"measureUnits\" : \":measureUnits\", \n    \"componentName\" : \":componentName\",\n    \"componentType\" : \":componentType\", \n  " +
                "  \"componentVersion\" : \":componentVersion\", \n    \"categoryName\": \":categoryName\", \n    \"enableAnalytics\": :enableAnalytics \n}";

        kpiRequestBody = kpiRequestBody.replace(":kpiName", kpiPojo.getKpiName());
        kpiRequestBody = kpiRequestBody.replace(":kpiIdentifier", kpiPojo.getKpiIdentifier());
        kpiRequestBody = kpiRequestBody.replace(":description", kpiPojo.getDescription());
        kpiRequestBody = kpiRequestBody.replace(":groupKpiIdentifier", kpiPojo.getGroupKpiIdentifier());
        kpiRequestBody = kpiRequestBody.replace(":dataType", kpiPojo.getDataType());
        kpiRequestBody = kpiRequestBody.replace(":kpiType", kpiPojo.getKpiType());
        kpiRequestBody = kpiRequestBody.replace(":clusterOperation", kpiPojo.getClusterOperation());
        kpiRequestBody = kpiRequestBody.replace(":rollupOperation", kpiPojo.getRollupOperation());
        kpiRequestBody = kpiRequestBody.replace(":instanceAggregation", kpiPojo.getInstanceAggregation());
        kpiRequestBody = kpiRequestBody.replace(":clusterAggregation", kpiPojo.getClusterAggregation());
        kpiRequestBody = kpiRequestBody.replace(":collectionIntervalSeconds", String.valueOf(kpiPojo.getCollectionIntervalSeconds()));
        kpiRequestBody = kpiRequestBody.replace(":measureUnits", kpiPojo.getMeasureUnits());
        kpiRequestBody = kpiRequestBody.replace(":componentName", kpiPojo.getComponentName());
        kpiRequestBody = kpiRequestBody.replace(":componentType", kpiPojo.getComponentType());
        kpiRequestBody = kpiRequestBody.replace(":componentVersion", kpiPojo.getComponentVersion());
        kpiRequestBody = kpiRequestBody.replace(":categoryName", kpiPojo.getCategoryName());
        kpiRequestBody = kpiRequestBody.replace(":enableAnalytics", String.valueOf(kpiPojo.getEnableAnalytics()));
        return kpiRequestBody;
    }

    private String prepareRequestBody(GroupKPIPojo groupKPIPojo) {

        String kpiGroupRequestBody = "{\n\"groupKpiName\": \":groupKpiName\",\n" +
                "\"groupKpiIdentifier\": \":groupKpiIdentifier\",\n" +
                "\"description\": \":description\",\n" +
                "\"kpiType\": \":kpiType\",\n" +
                "\"discovery\" : :discovery\n}";
        kpiGroupRequestBody = kpiGroupRequestBody.replace(":groupKpiName", groupKPIPojo.getGroupKpiName());
        kpiGroupRequestBody = kpiGroupRequestBody.replace(":groupKpiIdentifier", groupKPIPojo.getGroupKpiIdentifier());
        kpiGroupRequestBody = kpiGroupRequestBody.replace(":description", groupKPIPojo.getDescription());
        kpiGroupRequestBody = kpiGroupRequestBody.replace(":kpiType", groupKPIPojo.getKpiType());
        kpiGroupRequestBody = kpiGroupRequestBody.replace(":discovery", String.valueOf(groupKPIPojo.getDiscovery()));
        return kpiGroupRequestBody;

    }

    public boolean addGroupKPI(String identifier, GroupKPIPojo groupKPIPojo, TokenPojo tokenPojo) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        String kpiRequestBody = prepareRequestBody(groupKPIPojo);
        RequestBody body = RequestBody.create(mediaType, kpiRequestBody);
        String url = Constants.KPI_GROUP_URL.replace(":identifier", identifier);
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader("Authorization", tokenPojo.getAccess_token())
                .addHeader("Accept", "*/*")
                .addHeader("Content-Type", "text/plain")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == Constants.SUCCESS) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<KpiNamesPojo> getKPIs(TokenPojo tokenPojo, String accountId, int serviceId, int instanceId, int categoryId) {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        String url = Constants.KPI_URL_GET.replace(":identifier", accountId);
        url = url.replace(":serviceId", String.valueOf(serviceId));
        url = url.replace(":instanceId", String.valueOf(instanceId));
        url = url.replace(":categoryId", String.valueOf(categoryId));

        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .addHeader("Authorization", tokenPojo.getAccess_token())
                .build();
        Response response = null;
        List<KpiNamesPojo> kpis = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == Constants.SUCCESS) {
                ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    GenericResponse<KpiNamesPojo> data = objectMapper.readValue(response.body().string(), new TypeReference<GenericResponse<KpiNamesPojo>>() {
                    });
                    kpis = data.getData();
                    return kpis;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return kpis;
    }
}

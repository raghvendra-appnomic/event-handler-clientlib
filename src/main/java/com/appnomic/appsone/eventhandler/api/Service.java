package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.pojo.request.ServiceRequestPojo;
import com.appnomic.appsone.eventhandler.pojo.response.ServiceResponsePojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.appsone.eventhandler.util.APIConfigLoader;
import com.appnomic.appsone.eventhandler.util.CommonUtils;
import com.appnomic.appsone.eventhandler.util.Constants;
import com.appnomic.appsone.eventhandler.pojo.response.GenericResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service {

    public List<ServiceResponsePojo> getServices(TokenPojo tokenPojo, String identifier) {
        Map<String, String> replaceParams = new HashMap<>();
        replaceParams.put("identifier",identifier);

        Endpoint serviceEndpoint = APIConfigLoader.getEndpointById("service-get", replaceParams, tokenPojo);

        Request request = API.buildRequest(serviceEndpoint,null);

        Response response;
        List<ServiceResponsePojo> services = null;
        try {
            response = API.sendRequest(request);
            if (response.code() == Constants.SUCCESS) {
                ObjectMapper objectMapper = CommonUtils.getObjectMapperWithHtmlEncoder();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    GenericResponse<ServiceResponsePojo> data = objectMapper.readValue(response.body().string(), new TypeReference<GenericResponse<ServiceResponsePojo>>() {
                    });
                    services = data.getData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return services;
    }

    public boolean addService(String identifier, List<ServiceRequestPojo> services, TokenPojo tokenPojo) {
        Map<String, String> replaceParams = new HashMap<>();
        replaceParams.put("identifier",identifier);

        Endpoint serviceEndpoint = APIConfigLoader.getEndpointById("service-post", replaceParams, tokenPojo);
        boolean serviceAdded = false;

        if (serviceEndpoint != null) {

            MediaType mediaType = MediaType.parse(serviceEndpoint.getMediaType());

            String addRequestBody = prepareRequestBody(services);
            RequestBody body = RequestBody.create(mediaType, addRequestBody);

            Request request = API.buildRequest(serviceEndpoint,body);

            Response response;
            try {
                response = API.sendRequest(request);
                String responseBody = response.body().string();
                if (response.code() == Constants.SUCCESS) {
                    serviceAdded = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return serviceAdded;
    }

    private String prepareRequestBody(List<ServiceRequestPojo> services) {

        String prefix = "[\n   ";
        String suffix = "\n   ]";
        String body = "";
        List<String> srvcs = new ArrayList<>();
        for (ServiceRequestPojo service : services) {
            String mandatory = " {\n  \"name\":\":name\",\n  " +
                    " \"identifier\":\":identifier\"";

            mandatory = mandatory.replace(":name", service.getName());
            mandatory = mandatory.replace(":identifier", service.getIdentifier());
            if (service.getLayer() != null) {
                mandatory += " ,\n  \"layer\": \":layer\"";
                mandatory = mandatory.replace(":layer", service.getLayer());
            }
            if (service.getAppIdentifier() != null) {
                mandatory += " ,\n  \"appIdentifier\": \":appIdentifier\"";
                mandatory = mandatory.replace(":appIdentifier", service.getAppIdentifier());
            }
            if (service.getType() != null) {
                mandatory += " ,\n  \"type\": \":type\"";
                mandatory = mandatory.replace(":type", service.getType());
            }
            mandatory += " \n }";
            srvcs.add(mandatory);
        }
        body += prefix;
        for (String mandt : srvcs) {
            body += mandt + ",";
        }
        body = body.substring(0, body.length() - 1);
        body += suffix;
        return body;
    }


}

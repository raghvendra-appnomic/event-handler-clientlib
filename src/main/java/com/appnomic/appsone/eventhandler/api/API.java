package com.appnomic.appsone.eventhandler.api;

import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.util.APIConfigLoader;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class API {

    public static Response sendRequest(Request request) throws IOException {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.connectTimeout(APIConfigLoader.getConfig().getConnectTimeout(), TimeUnit.MINUTES);
        builder.readTimeout(APIConfigLoader.getConfig().getReadTimeout(), TimeUnit.MINUTES);
        builder.writeTimeout(APIConfigLoader.getConfig().getWriteTimeout(), TimeUnit.MINUTES);

        OkHttpClient client = builder.build();

        return client.newCall(request).execute();
    }

    public static Request buildRequest(Endpoint endpoint, RequestBody body) {
        Request.Builder request = new Request.Builder()
                .url(endpoint.getFinalUrl())
                .method(endpoint.getMethod(), body);

        Map<String, String> headers = endpoint.getHeaders();

        if (headers != null) {
            headers.forEach((headerName, headerValue) -> {
                if (headerValue.equalsIgnoreCase("Authorization")) {
                    request.addHeader(headerValue, endpoint.getToken().getAccess_token());
                } else {
                    request.addHeader(headerName, headerValue);
                }
            });
        }
        return request.build();
    }
}

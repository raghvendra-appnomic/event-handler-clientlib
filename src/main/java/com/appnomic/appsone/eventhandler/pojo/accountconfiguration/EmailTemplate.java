package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailTemplate {

    private int id;
    private String userId;
    private String subject;
    private String body;
    private boolean status;
    private String name;
    private int templateTypeId;
    private String templateType;
    private String toAddress;
    private String ccAddress;
    private String bccAddress;
    private String templateStatus;
}

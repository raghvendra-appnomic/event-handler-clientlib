package com.appnomic.appsone.eventhandler.pojo.response;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountConfiguration {

    private int id;
    private String accountId;
    private String accountName;
    private String publicKey;
    private List<AgentConfig> agentList;
    private List<Supervisors> supervisors;
    private List<WindowProfile> windowProfiles;
    private List<Controller> applicationList;
    private List<Services> serviceList;
    private List<TransactionConfig> transactionConfigs;
    private List<CompInstanceClusterConfig> compInstanceDetail;
    private List<ActionBean> forensicActions;
    private List<ActionBean> healActions;
    private List<ComponentKpiDetail> componentDetail;
    private List<RulesBean> rules;
    private List<Map<String, String>> tags;
    private List<UserPojo> users;
    private SmtpDetails smtpDetails;
    private SmsDetails smsDetails;
    private List<EmailTemplate> emailTemplates;
    private List<SMSTemplate> smsTemplates;
    private List<ConnectionDetails> connectionDetails;
    private List<NotificationPlaceholder> notificationPlaceholders;
    private MasterCustomNameList masterSubTypeCustomList;
    private List<NotificationSettingsBean> notificationSettings;
}

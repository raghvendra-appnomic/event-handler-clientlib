package com.appnomic.appsone.eventhandler.pojo;

import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Endpoint {
    String endpointId;
    String endpointUrl;
    Map<String, Boolean> pathParams;
    Map<String, String> headers;
    Map<String, String> mandatoryFields;
    Map<String, String> body;
    String mediaType;
    String method;
    String finalUrl;
    TokenPojo token;
}

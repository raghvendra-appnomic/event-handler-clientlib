package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompInstKpiProducerDetail {

    private int producerId;
    private String producerName;
    private String producerType;
    private int status;
    private int isCustom;
    private int isDefault;
    private String className;
    private int accountId;
    private int kpiProducerMappingId;
    private Map<String, String> typeDetails;
    private List<ProducerParameter> parameters;

}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class Services {
    private String id;
    private String serviceId;
    private String name;
    private List<ServiceConfigs> configurations;
    private Set<Integer> agentIds;
    private List<Map<String, String>> tags;
    private List<KPIThreshold> kpis;
    private List<MaintenanceDetailArgumentBean> maintenanceDetails;
}

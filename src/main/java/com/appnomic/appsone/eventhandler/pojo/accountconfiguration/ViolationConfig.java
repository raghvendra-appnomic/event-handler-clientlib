package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ViolationConfig {
    private int id;
    private String profileName;
    private int profileId;
    private int status;
    private String operation;
    private String minThreshold;
    private String maxThreshold;
    private String startTime;
    private String endTime;
    private String applicableTo;
    private int generateAnomaly;
    private String definedBy;
    private int severity;
}

package com.appnomic.appsone.eventhandler.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProducerPojo {
    private String name;
    private String description;
    private String kpiType;
    private int isGroupKpi;
    private String producerType;
}

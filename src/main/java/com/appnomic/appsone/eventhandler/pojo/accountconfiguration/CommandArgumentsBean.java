package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommandArgumentsBean {
    private int id;
    private String key;
    private String value;
    private String defaultValue;
    private String valueType;
    private String argumentType;
    private String isPlaceHolder;
}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestTypeDetailBean {

    private int id;
    private String firstSegment;
    private String lastSegment;
    private boolean completeURI;
    private int payloadTypeId;
    private String payloadTypeName;
    private List<PairDataBean> pairData;
    private int ruleId;
    private Timestamp createdTime;
    private Timestamp updatedTime;
    private int accountId;
    private String userDetails;
    private int firstUriSegments;
    private int lastUriSegments;
    private String completePattern;
    //private boolean segCompleteUriPattern;
    private String customSegments;
    private int httpMethodType;
    private int payloadType;
}

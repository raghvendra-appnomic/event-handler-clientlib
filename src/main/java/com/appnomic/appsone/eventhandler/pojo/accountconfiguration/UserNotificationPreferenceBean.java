package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"createdTime", "updatedTime"})
public class UserNotificationPreferenceBean {

    private int signalTypeId;
    private String signalType;

    private int signalSeverityId;
    private String signalSeverity;

    private int notificationTypeId;
    private String notificationType;

    private Timestamp createdTime;
    private Timestamp updatedTime;
}

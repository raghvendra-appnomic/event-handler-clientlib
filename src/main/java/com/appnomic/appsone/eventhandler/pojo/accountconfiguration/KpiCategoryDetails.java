package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KpiCategoryDetails {
    private int id;
    private Boolean isWorkLoad;
    private String name;
    private String categoryId;
}

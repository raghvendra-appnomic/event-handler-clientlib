package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDetails {

    private int applicationId;
    private String applicationName;
    private String applicationIdentifier;
    private int status;

    private List<UserNotificationPreferenceBean> notificationPreferences;
}

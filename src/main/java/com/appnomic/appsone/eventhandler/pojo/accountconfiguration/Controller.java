package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Controller {

    private String appId;
    private String name;
    private int controllerTypeId;
    private long timeOffset;
    private List<ServiceConfig> serviceDetails = new ArrayList<>();
    private List<NotificationPreferencesPojo> notificationPreferences = new ArrayList<>();
    private String identifier;
    private int status;
    private String createdBY;
    private String createdOn;
    private String updatedTime;
    private int accountId;
}

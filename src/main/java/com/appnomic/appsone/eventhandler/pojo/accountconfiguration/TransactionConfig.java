package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionConfig {

    private String txnId;
    private String txnName;
    private String appName;
    private String appIdentifier;
    private int id;
    private TxnGroupConfig txnGroupConfig;
    private List<Map<String, String>> tags;
    private List<TxnKPIViolationConfig> txnKpiViolationConfig = new ArrayList<>();

}

package com.appnomic.appsone.eventhandler.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupKPIPojo {
    String groupKpiName;
    String groupKpiIdentifier;
    String description;
    String kpiType;
    int discovery;
}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PairDataBean {

    private int id;
    private int pairTypeId;
    private String pairTypeName;
    private String paramKey;
    private String paramValue;
    private int ruleId;
    private int httpPatternId;
    private Timestamp createdTime;
    private Timestamp updatedTime;
    private int accountId;
    private String userDetails;

}

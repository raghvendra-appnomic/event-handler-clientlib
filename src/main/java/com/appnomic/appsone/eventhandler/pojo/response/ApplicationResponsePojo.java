package com.appnomic.appsone.eventhandler.pojo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationResponsePojo {
    int id;
    String name;
    String identifier;
    String lastModifiedOn;
    String lastModifiedBy;
    List<ServiceResponsePojo> services;
}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmsDetails {

    private int id;
    private String address;
    private int port;
    private String countryCode;
    private String protocolName;
    private String httpMethod;
    private String httpRelativeUrl;
    private String postData;
    private int isMultiRequest = 0;
    private List<SmsParameter> parameters;
    private Map<String, String> error = new HashMap<>();

    private static final int ADDRESS_LENGTH = 128;
    private static final int COUNTRY_CODE_LENGTH = 32;
    private static final int HTTP_URL_LENGTH = 512;
}

package com.appnomic.appsone.eventhandler.pojo.request;

import com.appnomic.appsone.eventhandler.pojo.response.ComponentInstanceResponsePojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceRequestPojo {

    private String name;
    private String identifier;
    private String layer;
    private String appIdentifier;
    private String type;
    private List<ComponentInstanceResponsePojo> instances;
    private String timezoneId;
}
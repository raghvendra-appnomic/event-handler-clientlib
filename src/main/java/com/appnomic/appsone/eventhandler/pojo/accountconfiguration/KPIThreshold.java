package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KPIThreshold
{
    private int id;
    private String updatedTime;
    private Map<String,List<ViolationConfig>> kpiViolationConfigMap;
}
package com.appnomic.appsone.eventhandler.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class APIConfiguration {
	private List<Endpoint> endPoints;
	private String baseUrl;
	private String endpointPrefix;
	private long connectTimeout;
	private long readTimeout;
	private long writeTimeout;
}
package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPojo {
    private String userDetailsId;
    private String userName;
    private String firstName;
    private String lastName;
    private String emailId;
    private String contactNumber;
    private int status;
    private int roleId;
    private int profileId;
    private int profileChange;
    private int emailEnabled;
    private int smsEnabled;
    private List<ApplicationDetails> applicationDetails;
    private UserTimezoneResponse timezoneDetail;

}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import java.util.HashMap;
import java.util.Map;


public enum ViolationType {

    SLOW_PERCENTAGE("Slow Percentage"),
    FAILURE_PERCENTAGE("Fail Percentage"),
    TOTAL_VOLUME("Total Volume"),
    RESPONSE_TIME("Response Time (ms)");

    private String configDBName;
    private static final Map<String, ViolationType> reverseLookupMap = new HashMap<>();

    ViolationType(String configDBName) {
        this.configDBName = configDBName;
    }

    public String getConfigDBName() {
        return configDBName;
    }

}

package com.appnomic.appsone.eventhandler.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryPojo {
    private String name;
    private String identifier;
    private boolean isWorkLoad;
}
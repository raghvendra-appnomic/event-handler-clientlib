package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaintenanceDetailArgumentBean {
    private int id;
    private String name;
    private String typeName;
    private Timestamp startTime;
    private Timestamp endTime;
    private boolean ongoing;
}
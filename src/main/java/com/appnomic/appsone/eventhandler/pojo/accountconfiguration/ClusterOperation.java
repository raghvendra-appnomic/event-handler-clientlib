package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

public enum ClusterOperation {

    AVERAGE("AVERAGE"),
    SUM("SUM"),
    ANY_OF("ANY_OF"),
    ALL_OF("ALL_OF"),
    MIN("MIN"),
    MAX("MAX"),
    LAST("LAST"),
    NONE("NONE");


    private String operation;

    ClusterOperation(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    @Override
    public String toString() {
        return operation;
    }
}

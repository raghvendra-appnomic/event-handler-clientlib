package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class MasterCustomNameList {
    private List<MstSubTypeData> signalSeverities;
    private List<MstSubTypeData> notificationTypes;
    private List<MstSubTypeData> signalTypes;

    public MasterCustomNameList() {
        signalSeverities = new ArrayList<>();
        notificationTypes = new ArrayList<>();
        signalTypes = new ArrayList<>();
    }
}

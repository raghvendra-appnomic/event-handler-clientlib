package com.appnomic.appsone.eventhandler.pojo.response;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.Attributes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ComponentInstanceResponsePojo {
    private int id;
    private String name;
    private String identifier;
    private String componentName;
    private String componentVersion;
    private String commonVersion;
    private List<String> serviceIdentifiers;
    private String parentIdentifier;
    private String parentName;
    private List<String> agentIdentifiers;
    private int discovery;
    private List<Attributes> attributes;
}
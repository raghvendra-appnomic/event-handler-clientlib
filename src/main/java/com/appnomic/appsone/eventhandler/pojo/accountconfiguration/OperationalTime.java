package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.DayOfWeek;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OperationalTime {

    private DayOfWeek day;
    private Boolean isBusinessHour;
    private int startHour;
    private int startMinute;
    private int endHour;
    private int endMinute;
}

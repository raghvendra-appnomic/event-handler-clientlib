package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ComponentKpiConfig {

    private String name;
    private int id;
    private String kpiType;
    private Boolean isGroup;
    private String groupName;
    private int groupId;
    private String kpiUnit;
    private ClusterOperation aggOperation;
    private ClusterOperation rollUpOperation;
    private String clusterAggType;
    private String instanceAggType;
    private String aliasName;
    private int availableForAnalytics;
    private Map<String, List<KpiViolationConfig>> kpiViolationConfigMap = new HashMap<>();
    private KpiCategoryDetails categoryDetails;
}

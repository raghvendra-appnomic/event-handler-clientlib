package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.*;
import java.sql.Timestamp;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommandDetailArgumentBean {

    private int id;
    @NonNull
    private int commandId;
    @NonNull
    private String argumentKey;
    @NonNull
    private String argumentValue;
    @NonNull
    private String defaultValue;
    @NonNull
    private int argumentTypeId;
    @NonNull
    private int argumentValueTypeId;
    private String userDetails;
    private Timestamp createdTime;
    private Timestamp updatedTime;
    private boolean placeHolder = false;

}

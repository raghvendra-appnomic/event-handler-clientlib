package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TxnKPIViolationConfig {

    private ViolationType txnKpiType;
    private String responseTimeType;
    private List<KpiViolationConfig> kpiViolationConfig = new ArrayList<>();
}

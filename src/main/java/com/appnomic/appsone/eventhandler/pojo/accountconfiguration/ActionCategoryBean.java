package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActionCategoryBean {

    private int id;
    private String name;
    private String categoryId;
    private int timeWindowInSecs;
    private String commandExecType;
    private String actionExecType;
    private String downloadType;
    private int retries;
    private int ttlInSecs;
    private ActionScriptsBean scriptDetails;
    @JsonIgnore
    private int actionExecTypeId;
    @JsonIgnore
    private int objectId;
    @JsonIgnore
    private String objectRefTable;

}

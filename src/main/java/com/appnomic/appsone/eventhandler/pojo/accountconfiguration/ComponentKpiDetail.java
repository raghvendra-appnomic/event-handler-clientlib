package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ComponentKpiDetail {

    private String componentId;
    private List<ComponentKpiConfig> kpis = new ArrayList<>();
    private String name;
    private String status;


}

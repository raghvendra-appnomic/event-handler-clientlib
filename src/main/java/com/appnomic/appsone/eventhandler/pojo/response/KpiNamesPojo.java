package com.appnomic.appsone.eventhandler.pojo.response;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.KpiAttribute;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KpiNamesPojo {
    int id;
    String name;
    String unit;
    String clusterOperation;
    String type;
    int isGroupKpi;
    String groupName;
    int isDiscovery;
    int groupId;
    int categoryId;
    String categoryName;
    long anomalyCount;
    private List<KpiAttribute> attribute;
}

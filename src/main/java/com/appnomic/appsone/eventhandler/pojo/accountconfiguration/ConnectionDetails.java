package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConnectionDetails {

    int id;
    int sourceId;
    String sourceIdentifier;
    String sourceRefObject;
    int destinationId;
    String destinationIdentifier;
    String destinationRefObject;
    int accountId;
    String userDetailsId;
}



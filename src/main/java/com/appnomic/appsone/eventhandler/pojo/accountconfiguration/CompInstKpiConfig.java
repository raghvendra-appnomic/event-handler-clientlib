package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompInstKpiConfig {

    private String name;
    private int id;
    private int compInstKpiId;
    private String kpiAliasName;
    private int collectionInterval;
    private int status;
    private String kpiUnit;
    private String kpiType;
    private Boolean isGroup;
    private String groupName;
    private int groupId;
    private int isCustom;
    private String attributeValue;
    private ClusterOperation aggOperation;
    private ClusterOperation rollUpOperation;
    private String clusterAggType;
    private String instanceAggType;
    private int defaultProducerId;
    private int discovery;
    private int availableForAnalytics;
    private String valueType;
    private String dataType;
    private List<CompInstKpiProducerDetail> producers;
    private Map<String, List<KpiViolationConfig>> kpiViolationConfigMap = new HashMap<>();
}

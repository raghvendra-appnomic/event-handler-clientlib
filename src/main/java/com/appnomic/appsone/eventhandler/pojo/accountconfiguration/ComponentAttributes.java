package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ComponentAttributes {
    private int attributeId;
    private String attributeName;
    private String attributeValue;
    private String attributeType;
    private int status;
    private int isCustom;
}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KpiViolationConfig {

    private String profileName;
    private String profileId;
    private String operation;
    private float minThreshold;
    private float maxThreshold;
    private Timestamp startTime;
    private Timestamp endTime;
}

package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationPreferencesPojo {
    private int applicationId;
    private String applicationName;
    private int signalTypeId;
    private String signalType;
    private int severityTypeId;
    private int notificationTypeId;
    private String severityType;
}

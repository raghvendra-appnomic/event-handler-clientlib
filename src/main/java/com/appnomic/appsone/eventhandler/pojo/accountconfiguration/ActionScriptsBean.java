package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActionScriptsBean {
    private int id;
    private String scriptName;
    private int timeOutInSecs;
    private String outputType;
    private String commandType;
    private List<CommandArgumentsBean> arguments;
}

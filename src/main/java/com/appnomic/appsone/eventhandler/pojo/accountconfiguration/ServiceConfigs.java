package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceConfigs {

    private int startCollectionInterval;
    private int endCollectionInterval;
    private int sorPersistence;
    private int sorSuppression;
    private int norPersistence;
    private int norSuppression;
}

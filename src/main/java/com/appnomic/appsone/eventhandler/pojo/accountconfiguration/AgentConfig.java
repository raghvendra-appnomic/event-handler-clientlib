package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgentConfig {

    private int id;
    private int physicalAgentId;
    private String agentId;
    private String agentName;
    private String agentMode;
    private int status;
    private int agentTypeId;
    private String agentTypeName;
    private String configOperationMode;
    private String dataOperationMode;
    private CollectionAgentParam collectionAgentParam;
    private List<Map<String, String>> tags;
    private List<Integer> compInstanceIds;
}

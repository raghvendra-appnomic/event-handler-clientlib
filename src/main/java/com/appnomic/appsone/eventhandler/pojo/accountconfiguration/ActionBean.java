package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActionBean {

    private int id;
    private String name;
    private String identifier;
    private String standardType;
    private String agentType;
    private String actionType;
    private int status;
    private List<ActionCategoryBean> categories;

    private int standardTypeId;
    private int agentTypeId;
    private List<Integer> categoryIds;
    private int executionTypeId;
    private int downloadTypeId;
    private int commandExecutionTypeId;
    private CommandDetailsBean commandDetails;
}


package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Supervisors {
    private int id;
    private String name;
    private String hostBoxName;
    private String supervisorId;
    private String supervisorType;
    private String version;
    private String hostAddress;
}

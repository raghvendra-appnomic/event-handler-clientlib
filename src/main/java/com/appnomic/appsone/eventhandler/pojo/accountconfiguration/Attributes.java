package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.ViolationConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Attributes {
    private String name;
    private String value;
    private List<ViolationConfig> violationConfig;
}
package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmtpDetails {

    private int id;
    private String address;
    private int port;
    private String username;
    private String password;
    private String security;
    private String fromRecipient;
    private Map<String, String> error = new HashMap<>();
    private static final int FIELD_LENGTH = 256;
}

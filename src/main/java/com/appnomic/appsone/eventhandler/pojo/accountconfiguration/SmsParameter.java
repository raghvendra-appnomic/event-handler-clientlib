package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SmsParameter {

    private int parameterId;
    private String parameterName;
    private String parameterValue;
    private int parameterTypeId;
    private String action;
    private Boolean isPlaceholder;
    private Map<String, String> error = new HashMap<>();
    private static final int PARAMETER_LENGTH = 128;
}

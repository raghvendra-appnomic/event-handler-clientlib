package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompInstanceClusterConfig {

    private String identifier;
    private String instanceId;
    private String instanceName;
    private String componentId;
    private String componentName;
    private Boolean isCluster;
    private int componentTypeId;
    private String componentType;
    private int componentVersionId;
    private String componentVersion;
    private int commonVersionId;
    private String commonVersion;
    private int status = 1;
    private List<Map<String, String>> tags;
    private List<String> clusterId = new ArrayList<>();
    private List<ServiceConfig> serviceDetails = new ArrayList<>();
    private List<String> applicationId = new ArrayList<>();
    private List<CompInstKpiConfig> kpis = new ArrayList<>();
    private List<ComponentAttributes> attributes = new ArrayList<>();



}

package com.appnomic.appsone.eventhandler.pojo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponsePojo {
    private String name;
    private int id;
    private String identifier;
    private String status;
    private String createdBy;
    private Timestamp createdOn;
    private Timestamp lastModifiedOn;
    private String lastModifiedBy;
    private String type = "NA";
    private Map<String, Object> application;
    private Map<String, Object> componentInstance;
    private Map<String, Object> inbound;
    private Map<String, Object> outbound;
    private boolean isMaintenance;
    private int txnCount;
    private List<String> tags;
    private String agentStatus;


}

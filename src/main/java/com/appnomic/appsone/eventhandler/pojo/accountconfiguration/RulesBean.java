package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RulesBean {
    private int id;
    private String name;
    private boolean enabled;
    private int order;
    private int ruleTypeId;
    private String ruleTypeName;
    private boolean defaultRule;
    private List<Map<String, String>> tags;
    private RegexTypeDetailBean regexTypeDetails;
    private RequestTypeDetailBean requestTypeDetails;
    private Timestamp createdTime;
    private Timestamp updatedTime;
    private String ruleFor;
    private int accountId;
    private int isDefault;
    private String userDetails;
    private int status;

    public String getServiceId(){
        if(tags == null){
            return null;
        }

        return tags.stream().filter(
                tag -> "Controller".equalsIgnoreCase(tag.get("type"))).findFirst()
                .map(tag -> (String) tag.get("key")).orElse(null);
    }

    public String getServiceIdentifier(){
        if(tags == null){
            return null;
        }

        return tags.stream().filter(
                tag -> "Controller".equalsIgnoreCase(tag.get("type"))).findFirst()
                .map(tag -> (String) tag.get("value")).orElse(null);
    }


}

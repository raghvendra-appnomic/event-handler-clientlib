package com.appnomic.appsone.eventhandler.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KPIPojo {
    String kpiName;
    String kpiIdentifier;
    String description;
    String groupKpiIdentifier;
    String dataType;
    String kpiType;
    String clusterOperation;
    String rollupOperation;
    String instanceAggregation;
    String clusterAggregation;
    int collectionIntervalSeconds;
    String measureUnits;
    String componentName;
    String componentType;
    String componentVersion;
    String categoryName;
    int enableAnalytics;
}

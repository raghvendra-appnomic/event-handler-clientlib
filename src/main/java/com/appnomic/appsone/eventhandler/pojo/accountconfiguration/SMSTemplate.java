package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SMSTemplate {

    private int id;
    private String userId;
    private String smsContent;
    private boolean status;
    private String name;
    private int templateTypeId;
    private String templateType;
    private List<String> mobileNumbers;
    private String templateStatus;
}

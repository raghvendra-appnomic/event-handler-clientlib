package com.appnomic.appsone.eventhandler.pojo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponse <T>{
    String message;
    String responseStatus;
    List<T> data;
}

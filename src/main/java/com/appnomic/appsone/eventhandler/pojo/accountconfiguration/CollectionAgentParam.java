package com.appnomic.appsone.eventhandler.pojo.accountconfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CollectionAgentParam {

    private int timeoutMultiplier;
    private String dataProtocol;
    private String dataAddress;
    private int dataPort;
    private boolean keepAlive;
    private boolean dataCompression;
}

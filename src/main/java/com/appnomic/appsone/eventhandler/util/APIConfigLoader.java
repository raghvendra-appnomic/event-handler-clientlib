package com.appnomic.appsone.eventhandler.util;

import com.appnomic.appsone.eventhandler.pojo.APIConfiguration;
import com.appnomic.appsone.eventhandler.pojo.Endpoint;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;
import java.util.stream.Collectors;

public class APIConfigLoader {

    private static final Logger log = LoggerFactory.getLogger(com.appnomic.appsone.eventhandler.util.APIConfigLoader.class);
    private static final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
    private static APIConfiguration config = null;

    static {
        load();
    }

    public static APIConfiguration getConfig() {
        return config;
    }

    private static void load() {
        try {
            InputStream configFile = APIConfigLoader.class.getClassLoader().getResourceAsStream(Constants.ENDPOINT_CONFIG_FILE_NAME);
            if (configFile == null) {
                log.debug("Current Directory: {} and Config file is supposed to be at {}",
                        System.getProperty("user.dir"), System.getProperty("user.dir") + configFile);
                throw new FileNotFoundException("Configuration File " + configFile + " not found.");
            }
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            config = objectMapper.readValue(configFile, APIConfiguration.class);

            log.info("ALL ENDPOINTS LOADED");

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException {}", e);
        } catch (JsonParseException e) {
            log.error("JsonParseException occurred {}", e);
        } catch (JsonMappingException e) {
            log.error("JsonMappingException occurred {}", e);
        } catch (IOException e) {
            log.error("IOException occurred {}", e);
        }
    }

    public static Endpoint getEndpointById(String endpointId, Map<String, String> replaceParams, TokenPojo token) {
        Endpoint endpoint = config.getEndPoints()
                .stream()
                .filter(e -> e.getEndpointId().equalsIgnoreCase(endpointId))
                .collect(Collectors.toList()).get(0);

        String finalUrl = APIConfigLoader.getEndpointUrl(endpoint, replaceParams);
        endpoint.setFinalUrl(finalUrl);
        endpoint.setToken(token);
        return endpoint;
    }

    public static String getTokenPayload(Endpoint tokenEndpoint) {
        StringBuffer payload = new StringBuffer();
        tokenEndpoint.getBody().forEach((field, value) -> {
            payload.append(field).append("=").append(value).append("&");
        });
        return payload.substring(0, payload.length() - 1);
    }

    public static String getEndpointUrl(Endpoint endpoint, Map<String, String> replaceParams) {
        StringBuffer url = new StringBuffer();

        if (endpoint.getEndpointId().equalsIgnoreCase("token")) {
            url.append(config.getBaseUrl()).append(endpoint.getEndpointUrl());
        } else {
            url.append(config.getBaseUrl()).append(config.getEndpointPrefix()).append(endpoint.getEndpointUrl());
        }

        String finalUrl = url.toString();
        if (replaceParams != null && replaceParams.size() != 0) {
            Map<String, Boolean> pathParams = endpoint.getPathParams();
            for (Map.Entry<String, Boolean> entry : pathParams.entrySet()) {
                if (entry.getValue()) {
                    finalUrl = finalUrl.replace(":" + entry.getKey(), replaceParams.get(entry.getKey()));
                }
            }
        }
        return finalUrl;
    }
}

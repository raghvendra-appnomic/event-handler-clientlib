package com.appnomic.appsone.eventhandler.util;

public class Constants {

    public static final String ENDPOINT_CONFIG_FILE_NAME = "endpoints.yaml";

    public static final int SUCCESS = 200;

    public static final String BASE_URL = "https://keycloak.appnomic:8443/";

    public static final String VERSION_PREFIX_CC = "appsone-controlcenter/v2.0/api/";
    public static final String VERSION_PREFIX_UI_SERVICE = "appsone-ui-service/v2.0/ui/";

    public static final String KPI_URL_POST = BASE_URL + VERSION_PREFIX_CC + "accounts/:identifier/kpis";
    public static final String KPI_URL_GET = BASE_URL + VERSION_PREFIX_UI_SERVICE + "accounts/:identifier/services/:serviceId/instances/:instanceId/categories/:categoryId/kpis";

    public static final String KPI_GROUP_URL = BASE_URL + VERSION_PREFIX_CC + "accounts/:identifier/kpi-groups";

    public static final String CATEGORY_URL_POST = BASE_URL + VERSION_PREFIX_CC + "accounts/:identifier/categories";
}

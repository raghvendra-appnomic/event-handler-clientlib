package com.appnomic.appsone.eventhandler;

import com.appnomic.appsone.eventhandler.api.*;
import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.Attributes;
import com.appnomic.appsone.eventhandler.pojo.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        // APIConfiguration apiConfig = APIConfigLoader.getConfig();


        ConfigAPI config = new ConfigAPI();

        List<AccountConfiguration> agentConfig = config.getAgentConfigData("e570de02-c585-4917-bbb7-5c97b35e-220");
        List<AccountConfiguration> txnConfig = config.getTxnConfigData();
        TokenPojo token = Token.getToken();

        Application application = new Application();


        Service service = new Service();

        String accountIdentifier = "d681ef13-d690-4917-jkhg-6c79b-1";


        List<ApplicationResponsePojo> list = application.getApplications(token, accountIdentifier);


/*        KPIPojo kpiPojo = KPIPojo.builder()
                .kpiName("Pod CPU Current").kpiIdentifier("POD_CPU_CUR_ID")
                .description("Node Current CPU usage").groupKpiIdentifier("None")
                .dataType("Float").kpiType("Core").clusterOperation("None")
                .rollupOperation("None").instanceAggregation("None")
                .clusterAggregation("None").collectionIntervalSeconds(60)
                .measureUnits("%").componentName("Node").componentType("Host")
                .componentVersion("1.12.0").categoryName("CPU")
                .enableAnalytics(1).build();

        KPI kpi = new KPI();

        boolean kpiAdded = kpi.addKPI(accountIdentifier, kpiPojo, token);*/



        /* log.info("Services available for the account {} are : {} ", list);

        ServiceRequestPojo servicePojo = ServiceRequestPojo.builder()
                .name(UUID.randomUUID().toString()).identifier(UUID.randomUUID().toString())
                .build();

        List<ServiceRequestPojo> serviceList = new ArrayList<>();

        serviceList.add(servicePojo);*/

         List<ServiceResponsePojo> servicesList = service.getServices(token, accountIdentifier);


        // boolean serviceAdded = service.addService(accountIdentifier, serviceList, token);

        Instances instances = new Instances();
        List<ComponentInstanceResponsePojo> componentInstances = instances.getInstances(token, accountIdentifier, 16);

        List<String> serviceIdentifiers = new ArrayList<>();

        serviceIdentifiers.add("4f51f8b0-b6c9-4375-8729-b21bc6d88888");

        List<ComponentInstanceResponsePojo> componentInstancesToAdd = new ArrayList<>();

        Attributes attribute = new Attributes();
        attribute.setName("HostAddress");
        attribute.setValue("192.168.0.35");
        List<Attributes> attributes = new ArrayList<>();
        attributes.add(attribute);

/*        ComponentInstanceResponsePojo comp1 = ComponentInstanceResponsePojo.builder()
                .name(UUID.randomUUID().toString()).identifier(UUID.randomUUID().toString())
                .componentName("AIX").componentVersion("6.1")
                .attributes(attributes)
                .serviceIdentifiers(serviceIdentifiers).build();

        componentInstancesToAdd.add(comp1);

        boolean instanceAdded = instances.addInstance(accountIdentifier, componentInstancesToAdd, token);*/


        log.debug("Inside main() method.");
    }
}
